Información general
Agent Tesla es un tipo de malware de la familia de los keyloggers. Fue identificado por 
primera vez en 2014 y se encuentra escrito en VB.NET. Se distribuye a trav�s de correos
electr�nicos enga�osos y su objetivo es obtener informaci�n confidencial de las v�ctimas, 
incluyendo credenciales financieras, nombres de usuario, contrase�as, informaci�n del
portapapeles y capturas de pantalla. La infecci�n ocurre al ejecutar un archivo que contiene
el c�digo da�ino. Una vez infectado, el malware puede obtener persistencia en el sistema y
extraer contrase�as de los navegadores y otras aplicaciones. La muestra analizada por el 
CCN-CERT es un binario con formato PE para arquitecturas de 32 bits de sistemas operativos 
Windows y su firma SHA-1 es 9BF5E7EC3F335B9C1B8D80695BC7976B90A31764, sin embargo,
no fue posible encontrar la muestra en los repositorios "limpios" ni por ninguna p�gina de 
internet, debido a esto se decide realizar la subida al repositorio de una muestra descargada de 
Malware Bazar, la cual ha sido identificada recientemente por ser otro archivo da�ino del mismo
Malware o estar relacionado con este.

------Analisis----------

El análisis comienza con la categorización de una muestra de archivo desconocida. Tras lanzar un archivo, 
se descubre que es un PE32, un ejecutable para Windows. Se realiza un escaneo del archivo con el antivirus 
open source ClamAV, que no lo detecta como malicioso. La muestra se sube a VirusTotal,
una plataforma que ofrece análisis de malware, y se descubre que se trata de AgentTesla, un agente malicioso.

Luego, se procede a analizar la muestra con una sandbox, AnyRun. Esta herramienta permite detectar que la 
muestra accede a varios recursos del usuario, como direcciones IP y carpetas en red, y gana persistencia en el sistema.
También se examinan los strings del archivo con el comando "strings", y se descubre información interesante, como que
el programa está diseñado para ejecutarse solo en sistemas operativos Windows y que no puede ejecutarse en un entorno DOS.  

El análisis continúa con el uso de herramientas de extracción y descompresión, como Binwalk y Foremost. 
Se descubren varias imágenes y un ejecutable comprimido dentro de la muestra. Al abrir las imágenes, se revela 
que son de una galería de fotos de la revista Playboy. Se descomprime la parte comprimida del archivo, pero no se 
encuentran strings interesantes ni cabeceras adicionales. 

Finalmente, se genera una regla Yara para la detección del malware basada en los strings relacionados con Pokémon 
encontrados en la muestra. Al aplicar la regla al archivo, se detecta el malware. 
