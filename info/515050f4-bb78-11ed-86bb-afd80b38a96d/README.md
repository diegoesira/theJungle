El gusano Kido.Worm, también conocido como Conficker o Downup, es un malware que se propagó ampliamente en el mundo entre 2008 y 2009.
Este gusano aprovechaba una vulnerabilidad en el servicio de servidor RPC de Windows para infectar sistemas
y luego desactivaba el software antivirus del sistema. Una vez dentro del sistema,
Kido descargaba software malicioso adicional en el equipo infectado, convirtiéndolo en parte de una botnet controlada remotamente.
Kido también utilizaba una técnica de propagación que se basaba en la propagación a través de unidades USB y otros dispositivos de almacenamiento extraíbles.
A pesar de los esfuerzos para detener su propagación, Kido continuó siendo un problema durante varios años, y algunas variantes aún se detectan en la actualidad. 

Esta muestra se ha obtenido del HoneyPot en la red.

    Se ha realizado un procedimiento de análisis de una muestra de malware. En primer lugar, se ejecutó un archivo para determinar su tipo,
 resultando ser un archivo ejecutable para Windows. Luego se utilizó ClamScan, un antivirus open source basado en firmas, para detectar si se trataba de un archivo malicioso.
 El resultado mostró que efectivamente era un gusano.

Para contrastar la información obtenida, se consultó VirusTotal, una plataforma que ofrece el análisis de archivos en busca de malware. Este análisis también detectó
 que se trataba de un gusano conocido como Kido.Worm.

 A continuación, se ejecutó la muestra en AnyRun, una plataforma de análisis de malware, para recopilar más información. Sin embargo, la ejecución falló,
 sugiriendo que podría tratarse de una librería dinámica, tal y como había señalado VirusTotal.

 Para confirmar esta sospecha, se utilizó Binwalk, que reveló que efectivamente se trataba de un ejecutable. Se volvió a ejecutar la muestra en AnyRun 
sin la opción de aplicar una extensión automática, lo que provocó un crash de la Sandbox.

Luego se utilizó la herramienta "strings" para buscar información de las cadenas de caracteres en el archivo, lo que mostró la presencia de funciones relacionadas 
con las bibliotecas dinámicas ol32 y Shell32. La primera se utiliza para la interacción de hilos de procesos y la expansión del gusano, mientras que la segunda
se puede utilizar para acciones maliciosas como la manipulación de archivos y carpetas, la modificación de la configuración del sistema, 
la obtención de información del sistema y la ejecución de archivos maliciosos. También se observó que la muestra había sido diseñada para ser ejecutada en sistemas
operativos específicos, en este caso Windows.

Después se utilizó Rabin2 para obtener información adicional del archivo, lo que mostró tres funciones que podrían alterar la
 apariencia gráfica del entorno del usuario o incluso detectar si se encuentra en un entorno de prueba. La herramienta grep se utilizó para buscar URLs,
 IPs y rutas incrustadas en el código, pero no se encontró ninguna información útil.

Finalmente, se utilizó Foremost para analizar el ejecutable y detectar cualquier archivo incrustado. El resultado mostró que la muestra contenía una dll para su
ejecución, lo que fue detectado por ClamScan como un archivo malicioso. Además, la alta entropía obtenida sugiere que hay archivos comprimidos en su interior.
Para detectar este tipo de archivos, se creó una regla YARA que hace referencia a las funciones detectadas anteriormente.

