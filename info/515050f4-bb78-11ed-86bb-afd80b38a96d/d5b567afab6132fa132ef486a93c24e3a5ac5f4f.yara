rule buscar_funciones_dll {
	strings:
		$funciones1 = "GetStretchBltMode"
		$funciones2 = "GdiGetBatchLimit"
		$funciones3 = "GetROP2"
		$dlls1 = "GDI32.dll"
		$dlls2 = "ole32.dll" 
		$dlls3 = "SHELL32.dll"
	condition:
		$funciones1 and $funciones2 and $funciones3 and $dlls1 and $dlls2 and $dlls3
}
